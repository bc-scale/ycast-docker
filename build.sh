#! /usr/bin/env bash

# You need to create a docker buildx multiarch builder first:
# docker buildx use $(docker buildx create --name multiarch-builder --driver docker-container --platform linux/amd64,linux/amd64/v2,linux/arm64/v8,linux/arm/v7,linux/arm/v6,linux/arm/v5,linux/386,linux/mips64le,linux/mips64,linux/ppc64le,linux/riscv64,linux/s390x,windows/amd64)


# Then build the multiarch image and push it directly to the repo.
(
archs="linux/arm64,linux/amd64,linux/386"
d=Dockerfile
t=registry.gitlab.com/tjuerges/ycast-docker
now=$(date -u +'%F-%H.%M.%S')
docker buildx build --platform ${archs} -f ${d} -t ${t}:${now} --push .
)
